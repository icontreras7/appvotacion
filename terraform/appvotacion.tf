provider "azurerm" {
    subscription_id = "a70905da-4b30-4bcd-a348-159681635992"
    client_id       = "a70da603-0cfa-4f69-b011-957c37659a3c"
    client_secret   = "abbb2bc9-d6d5-4337-8524-b7730e8fbca9"
    tenant_id       = "72f988bf-86f1-41af-91ab-2d7cd011db47"
}


resource "azurerm_resource_group" "aci-rg" {
  name     = "vapdeveu1rgr003"
  location = "eastus"
}

resource "azurerm_container_service" "acs-k8s" {
  name                   = "vapdeveu1acs003"
  location               = "${azurerm_resource_group.aci-rg.location}"
  resource_group_name    = "${azurerm_resource_group.aci-rg.name}"
  orchestration_platform = "Kubernetes"

  master_profile {
    count      = 1
    dns_prefix = "vapdeveu1rgr003-master"
  }

  linux_profile {
    admin_username = "eladmin"

    ssh_key {
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAiphtSZE1nr4OqUNcSce3xZPIAAIaEJkAU4WQ93/tHyZwYmMECA9M6yGCE5etLW9F4L5a3Rd+j7qPh1fYNQdNDUs779LGjZRtVaf6psMmU2/BNqYEgfAgvZNbPFNnDThv6POJQyTIioY3bERpdyMcfXMX7xjJdfcJgnC92oemeZ7QDY8JlMcCMKst9J/FuwsagqI+IkWsNXXS32KTTjHEBSQjbxNg7MHKEMzePYR2Pg95qgy+F8WFy57wKd/r4t4Flej0dcexW/DLCieTaojLEJvxT6DXquPoNKPy5bHkro/4VvnbQKERsFfBWElOgUJ6GqhhqEf9li7WxbxhhX+59w=="
    }
  }

  agent_pool_profile {
    name       = "default"
    count      = 2
    dns_prefix = "vapdeveu1rgr003-agent"
    vm_size    = "Standard_D2"
  }

  service_principal {
    client_id       = "a70da603-0cfa-4f69-b011-957c37659a3c"
    client_secret   = "abbb2bc9-d6d5-4337-8524-b7730e8fbca9"
  }

  diagnostics_profile {
    enabled = false
  }

  tags {
    Environment = "Desarrollo"
  }
}

resource "azurerm_container_registry" "acr" {
  name                = "vapdeveu1acr003"
  resource_group_name = "${azurerm_resource_group.aci-rg.name}"
  location            = "${azurerm_resource_group.aci-rg.location}"
  admin_enabled       = true
  sku                 = "Standard"
  tags {
    Environment = "Desarrollo"
  }
}
